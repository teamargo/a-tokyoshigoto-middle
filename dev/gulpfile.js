var gulp = require('gulp');
var sass = require('gulp-ruby-sass');
var sourcemaps = require('gulp-sourcemaps');
var spritesmith = require('gulp.spritesmith');
var browser = require('browser-sync');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var pleeease = require('gulp-pleeease');
var rename = require('gulp-rename');
var changed = require('gulp-changed');
var ftp = require('gulp-ftp');
var cache = require('gulp-cached');
var jade = require('gulp-jade');
var prettify = require('gulp-prettify');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var gutil = require('gulp-util');
var ftp = require('vinyl-ftp');
var autoprefixer = require('gulp-autoprefixer');

// server
gulp.task('server', function(){
	browser.init({
		proxy: "tokyoshigoto-middle.dev"
	})
});

gulp.task('reload', function(){
	gulp.src('../**/*.html','../**/*.inc')
		.pipe(browser.reload({stream:true}))
})

// sassのコンパイル
gulp.task('sass', function() {
	sass( "**/*.scss" , { 
		style: 'compressed',
		sourcemap: true,
		emitCompileError: true,
		compass: false
	})
	.on('error', notify.onError(function (error) {
		return "Error: " + error.message;
	}))
	.pipe(autoprefixer({
      browsers: ['last 2 versions'],
  }))
	.pipe(sourcemaps.init())
	.pipe(pleeease({
		"minifier": false
	}))
	.pipe(sourcemaps.write())
	.pipe(gulp.dest('../'))
	.pipe(browser.reload({stream:true}))
});

// 画像圧縮
// gulp.task('img', function(){
// 	gulp.src('../**/*.+(jpg|jpeg|png|gif|svg)')
// 		.pipe(cache())
// 		.pipe(imagemin({
// 			progressive: true,
// 			use: [pngquant()]
// 		}))
// 		.pipe(gulp.dest( '../' ));
// });

gulp.task('dist-copy', function(){
	gulp.src('../**/*.+(inc|css|php|js)')
	.pipe(gulp.dest('../'))
});

// 監視
gulp.task('watch', function(){
	gulp.watch('../**/*.+(html|inc|js)').on('change', browser.reload);
	gulp.watch('../**/*.+(png|jpg|jpeg|gif)')
	gulp.watch('**/*.+(scss)',['sass'])
	gulp.watch('../**/*.+(inc|css|php|js|css)',['dist-copy'])
});

// デフォルトタスク
gulp.task('default',['watch','server','dist-copy']);
gulp.task('all',['sass','dist-copy']);


