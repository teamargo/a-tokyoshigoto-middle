$(document).ready(function(){
  var window_size = $(window).width();

   jQuery.easing.myEasing4 = function(x, t, b, c, d) {
        return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
  };

  // ボックスの高さを合わせる
  $('.cmp-flow_box--item,.introduction_section--card,.cmp-step_box--step,.seminar_list--table_item,.cmp-announce_blk--list,.search_menu--link,.sort--item').matchHeight({
    byRow: true,
  });


  // セミナーを探す
  $('.page_head--btns_link-seminar').on('click', function() {
    $('.search_menu').slideToggle(300);
    $(this).toggleClass('page_head--btns_link-seminar-open');
    return false;
  });

  $('.search_menu--close').on('click', function() {
    $('.search_menu').slideToggle(300);
    $('.page_head--btns_link-seminar').toggleClass('page_head--btns_link-seminar-open');
  })

  // youtube動画　クリックで差し替え
  // var video = $('.youtube_btn').html
  $('.youtube_btn').on('click',function(){
    $(this).html('<iframe class="frame_youtube" width="220" height="125" src="https://www.youtube.com/embed/ISuy9qXlZ8Q?autoplay=1" frameborder="0" allowfullscreen></iframe>')
    return false;
  })

if(window_size > 600) {
  // seminar index overlay
  $('.basic_section--flow_item').on('click',function(){
    $(this).next().fadeToggle(300);
    $('.overlay_bg').fadeToggle(300);
  })
  $('.basic_section--overlay_close, .overlay_bg').on('click',function(){
    $('.basic_section--overlay_close').parent().fadeOut(300);
    $('.overlay_bg').fadeOut(300);
  })
  $('.seminar_tbl--link_overlay').on('click',function(){
    $(this).next().next().fadeToggle(300);
    $('.overlay_bg_target').fadeToggle(300);
    return false;
  })
  $('.seminar_tbl--overlay_close, .overlay_bg_target').on('click',function(){
    $('.seminar_tbl--overlay_close').parent().fadeOut(300);
    $('.overlay_bg_target').fadeOut(300);
  })
  

  // news page image popup
  $('.news_page--pic').on('click',function(){
    $('.news_page--overlay').css({"display":"block"});
    var pic_h = $('.news_page--overlay_pic').height();
    setTimeout(function(){
      var w_h = $(window).height();
      var pic_top = (w_h - pic_h)/2;
      $('.news_page--overlay').css({"opacity":"1"});
      $('.news_page--overlay_pic').css({"marginTop":pic_top + "px"});
    },100);
  });
  $('.news_page--overlay').on('click',function(){
    $(this).css({"opacity":"0","display":"none"});
  });


  // telephone link none
  var ua = navigator.userAgent;
    if( ua.indexOf('iPhone') < 0 && ua.indexOf('Android') < 0){
        $('a[href^="tel:"]').css( 'cursor', 'default' ).click(function(event){
            event.preventDefault();
        });
    }
}

  // seminar event announce
  $('.seminar_list_section--btn-popup,.event_section--btn-popup').on('click', function(){
    var scroll_y = $(window).scrollTop();
    var scroll_mt = 100 + scroll_y;
    $('.cmp-announce_inner').css({"marginTop":scroll_mt + "px"});
    $('.cmp-announce').fadeIn(200);
    return false;
  })
  $('.cmp-announce--close, .cmp-announce').on('click', function(){
    $('.cmp-announce').fadeOut(200);
  })


  // seminar about reservation
  $(".cmp-step_box--step_ttl").on('click',function(){
    $(this).next().slideToggle(300);
    $(this).toggleClass('cmp-step_box--step_ttl-open');
  })


  // faq アコーディオン
  $('.faq_list--txt-question').on('click', function() {
    $(this).parent().next('.faq_list-answer').slideToggle(300);
    $(this).parent().toggleClass('faq_list-question_open');
  })

  $('.faq_list--txt-close').on('click', function() {
    $(this).parent().parent('.faq_list-answer').slideToggle(300);
    $('.faq_list-question').removeClass('faq_list-question_open');
  })

  // support_menu アコーディオン
  $('.support_menu_section--btn-seminar').on('click',function(){
    $(this).next('div').slideToggle(300);
    $(this).toggleClass('support_menu_section--btn-open');
    return false;
  })

  var w_h = $(window).height();
  var target = $(window).scrollTop();

  var topBtn = $('.page_top');
  topBtn.hide();
  $(window).scroll(function(){
  if($(this).scrollTop() > target + w_h/4) {
    topBtn.fadeIn();
  } else {
    topBtn.fadeOut();
  }
  });

  $('a[href^="#"]').click(function(){
    var speed = 400;
    var href = $(this).attr("href");
    var target = $(href == "#" || href == "" ? 'html' : href);
    var position = target.offset().top;
    $('body,html').animate({scrollTop:position}, speed, 'swing');
    return false;
  })

  // only smart phone
  if(window_size < 600) {
    $('.hamburger--btn').on('click',function(){
      $('.gl_nav').fadeToggle(100);
      $('.nav_blk').toggleClass('nav_blk-open');
      $('.logo').toggleClass('logo-open');
      $(".nav_blk_btn").fadeToggle(100);
    });

  $('.nav_blk_btn--item.nav_blk_btn--item-seminar').on('click', function() {
    $('.search_menu_sp').fadeToggle(300);
    $(".nav_blk").toggleClass("nav_blk-open_hide");
    $(".hamburger").css({"z-index":"-1"});
    return false;
  });
  $('.search_menu_sp').on("click",function(){
    $('.search_menu_sp').fadeToggle(300);
    $(".nav_blk").removeClass("nav_blk-open_hide");
    $(".hamburger").css({"z-index":"initial"});
  })

  $('.basic_section--flow_item').on('click', function() {
    $(this).next('.basic_section--overlay').slideToggle(300);
    $(this).toggleClass('.basic_section--overlay-open');
  })

  $('.basic_section--overlay_close').on('click', function() {
    $(this).parent('.basic_section--overlay').slideToggle(300);
    $('.basic_section--overlay').removeClass('.basic_section--overlay-open');
  })

  $('.target_seminar--ttl').on('click',function(){
    $(this).next('.target_seminar--body').slideToggle(300);
    $(this).toggleClass('target_seminar--ttl-open');
  })

  $('.sort--toggle_btn').on('click', function() {
    $('.sort--list').slideToggle(300);
    $(this).toggleClass('sort--toggle_btn-open');
  })

  // seminar event announce
  $('.seminar_list_section--btn-popup,.event_section--btn-popup').on('click', function(){
    var scroll_y = $(window).scrollTop();
    var scroll_mt = 50 + scroll_y;
    $('.cmp-announce_inner').css({"marginTop":scroll_mt + "px"});
    $('.cmp-announce').fadeIn(200);
    return false;
  })
  $('.cmp-announce--close, .cmp-announce').on('click', function(){
    $('.cmp-announce').fadeOut(200);
  })

  // event banner
  $('.event_bnr--bnr_img').on('click', function(){
    var scroll_y = $(window).scrollTop();
    $('.event_bnr--modal_inner').css({"marginTop":scroll_y + "px"});
    $('.event_bnr--modal').fadeIn(200);
  })
  $('.event_bnr--modal_close').on('click', function(){
    $('.event_bnr--modal').fadeOut(200);
  })
  $('.event_bnr--profile_ttl').on('click',function(){
    $('.event_bnr--profile_description').slideToggle(200);
    $(this).toggleClass('event_bnr--profile_ttl-open');
  })
  $('.event_bnr--profile_description span').on('click', function() {
    $('.event_bnr--profile_description').slideToggle(200);
    $('.event_bnr--profile_ttl').removeClass('event_bnr--profile_ttl-open');
  })

  // support menu top image lead
  var alt_txt = $('.ctg_visual--ttl img').attr("alt");
  $(".ctg_visual--ttl_txt").html(alt_txt);
  
  }

});
