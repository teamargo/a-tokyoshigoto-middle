$(document).ready(function(){
  var categoryArray = [], //カテゴリ配列
    activeCatArray = [];//アクティブカテゴリ

  $.ajax({
    type: 'GET',
    url: 'https://tokyo-jobcenter.herokuapp.com/special_site/middle/seminars?callback=callback',
    dataType: 'jsonp',
    jsonpCallback: 'callback',
    success: function(data){
      var len = data.length;
      for(var i=0; i<len; i++){

        // jsonファイルを元にtableに当て込み
        $("#event_list").append(
          '<div class="seminar_list--box" data-target="' + data[i].seminar.themes + '" data-number="'+ data[i].seminar.name + '" id="'+ data[i].seminar.name.slice(0,2) +'">' +
            '<ul class="seminar_stamp seminar_stamp-category">' +
              '<li class="seminar_stamp--item seminar_stamp--item-ttl">' + data[i].seminar.name + '</li>' +
              '<li class="seminar_stamp--item seminar_stamp--item-content">' + data[i].seminar.category + '</li>' +
            '</ul>' +
            '<div class="seminar_list--ttl">' +
              '<p class="seminar_list--ttl-lead">' + data[i].seminar.lead +
              '<h4>' + data[i].seminar.title + '</h4>' +
            '</div>' +
            '<dl class="seminar_list--content">' +
              '<dt class="seminar_list--content_ttl">' + '<span class="seminar_list--content_ttl-date">開催日時</span><br>' + 
              '<span class="seminar_list--content_ttl-cal">' + data[i].start_at.slice(0,10) + '</span>' + '<br>' + data[i].start_at.slice(11,16) + '</dt>' +
              '<dd class="seminar_list--content_txt">' + data[i].seminar.outline + '</dd>' +
              '<dd class="seminar_list--content_ttl seminar_list--content_op">' + '<a class="seminar_list--content_op-link" href="/contact/seminar.html" target="_blank">' + 'お申し込みは<br>こちら' + '</a>' + '</dd>' +
            '</dl>' +
            '<ul class="seminar_stamp seminar_stamp-term">' +
              '<li class="seminar_stamp--item seminar_stamp--item-ttl">' + '実施時間' + '</li>' +
              '<li class="seminar_stamp--item seminar_stamp--item-content span_time">' + data[i].seminar.schedule + '</li>' +
            '</ul>' +
            '<ul class="seminar_list--table_list">' +
              '<li class="seminar_list--table_item">' +
                '<dl class="seimnar_list--table_in">' +
                  '<dt class="seminar_list--table_in_ttl">' + '対象者' + '</dt>' +
                  '<dd class="seminar_list--table_in_data">' + data[i].seminar.target + '</dd>' +
                '</dl>' +
              '</li>' +
              '<li class="seminar_list--table_item seminar_list--table_item">' +
                '<dl class="seimnar_list--table_in">' +
                  '<dt class="seminar_list--table_in_ttl">' + 'テーマ' + '</dt>' +
                  '<dd class="seminar_list--table_in_data">' + data[i].seminar.themes + '</dd>' +
                '</dl>' +
              '</li>' +
              '<li class="seminar_list--table_item seminar_list--table_item">' +
                '<dl class="seimnar_list--table_in">' +
                  '<dt class="seminar_list--table_in_ttl">' + '開催場所' + '</dt>' +
                  '<dd class="seminar_list--table_in_data">' + data[i].address + '</dd>' +
                '</dl>' +
              '</li>' +
              '<li class="seminar_list--table_item seminar_list--table_item">' +
                '<dl class="seimnar_list--table_in">' +
                  '<dt class="seminar_list--table_in_ttl">' + '申込方法' + '</dt>' +
                  '<dd class="seminar_list--table_in_data">' + data[i].seminar.entry_method + '</dd>' +
                '</dl>' +
              '</li>' +
              '<li class="seminar_list--table_item seminar_list--table_item-md">' +
                '<dl class="seimnar_list--table_in">' +
                  '<dt class="seminar_list--table_in_ttl">' + '申込期間' + '</dt>' +
                  '<dd class="seminar_list--table_in_data">' + data[i].apply_start_on + '〜' + '</dd>' +
                '</dl>' +
              '</li>' +
              '<li class="seminar_list--table_item seminar_list--table_item-sml">' +
                '<dl class="seimnar_list--table_in">' +
                  '<dt class="seminar_list--table_in_ttl">' + '定員' + '</dt>' +
                  '<dd class="seminar_list--table_in_data">' + data[i].attender_number + '名</dd>' +
                '</dl>' +
              '</li>' +
            '</ul>' +
          '</div>'

        );

      // カテゴリー分けするために、重複しないカテゴリーをプッシュ
        // 同時にカテゴリーボタンを生成
        // var catItem = data[i].seminar.themes.toString().replace(/\,/g,'，');
        // if(categoryArray.indexOf(catItem)<0){
        //   categoryArray.push(catItem);
        //   $('#sort').append(
        //     '<li class="sort--item"><a class="sort--link" data-target="' + catItem + '">' + catItem + '</a></li>'
        //   );
        // }

        //カテゴリー状態がtrueの時は受付中テキスト修正
        if(data[i].multi_appliable == true ) {
          $('.reception').eq(i).html('受付中').addClass('on');
        } else {
          $('.reception').eq(i).html('受付終了').addClass('off');
        }
      }


      //すべてのセミナーが選択された状態に初期化
      //ローディング遅延は考慮していませんので、必要であればタイムアウトで処理してください
      activeCatArray = categoryArray.slice();
    }
  });


  // watch "Location Hash" and control presence by indication
  (function analysisOfHash() {
    setTimeout(function(){
      var hash = $(location).attr("hash");
      // item to display
      var targetItem = $("#event_list .seminar_list--box");
      // item to le you display it
      var linkItem = $(".sort--item");
      for(var f = 0, len = linkItem.length; f<len; f++ ) {
        var itemHash =  "#" + linkItem.eq(f).find('a').attr("data-target");
        if( itemHash == hash ) {
          linkItem.eq(f).find('a').addClass("current");
          targetItem.css({"display":"none"});
          for( var x = 0, len = $("#event_list").find('div').length; x<len; x++) {
            if( targetItem.eq(x).attr("data-target") == linkItem.eq(f).find('a').attr("data-target") ) {
              // targetItem.eq(x).css({"display":"table"});
            }
          }
        }
      }
    },1000);
  })();

  // nullの値を変換
  $(window).on('load', function(){
    var item = $('.seminar_list--box');
    var box_len = item.length;
    for(var i = 0; i<box_len; i++) {
      var span_time = item.eq(i).find(".span_time").html();
      if( span_time == 'null') {
        $(".span_time").eq(i).text('ー');
      }
    }
  });

  // EVとOPだけ表示する
  $(window).on('load',function(){
    (function eventlist(){
      var item = $('.seminar_list--box');
      var box_len = item.length;
      for( var i = 0; i<box_len; i++ ) {
        var target = item.eq(i).find(".seminar_stamp--item-content").html();
        if( target == "企業イベント（EV）" || target == "公募セミナー（OP）") {
          item.eq(i).css({"display":"block"});
        } else {
          item.eq(i).remove();
        }
      }
    })();
  })

  $(window).on('load',function(){
    (function lochash() {
      var hash = $(location).attr('hash');
      var item = $(".seminar_list--box");
      var ary = [];
      var s = 0;
      function str_count(all, part) {
        return(all.match(new RegExp(part,'g')) || []).length;
      }
      var count = str_count(hash,"#");

      if( hash ) {
        for( var i = 0; i < count; i++ ){
          ary.push( hash.substring(s,s+9) );
          s = s+9;

          if( i == count-1 ) {
            item.css({'display':'none'});
            for( var i = 0; i<item.length; i++ ) {
              if( '#' + item.eq(i).attr('data-number') == ary[0] || '#' + item.eq(i).attr('data-number') == ary[1] || '#' + item.eq(i).attr('data-number') == ary[2] || '#' + item.eq(i).attr('data-number') == ary[3] || '#' + item.eq(i).attr('data-number') == ary[4] || '#' + item.eq(i).attr('data-number') == ary[5] || '#' + item.eq(i).attr('data-number') == ary[6] || '#' + item.eq(i).attr('data-number') == ary[7] || '#' + item.eq(i).attr('data-number') == ary[8] || '#' + item.eq(i).attr('data-number') == ary[9] ) {
                item.eq(i).css({'display':'block'});
              }
            }
          }
        }
      }
    })();
  })

  // OPとEVの申し込みボタン表示
  $(window).on('load',function(){
    (function optag(){
      var box_len = $(".seminar_list--box").length;
      for( var i = 0; i<box_len; i++) {
        var target_id = $(".seminar_list--box").eq(i).attr("id");
        if( target_id == "OP" ) {
          $(".seminar_list--content_op").eq(i).css({"display":"table-cell"});
        } else if( target_id == "EV") {
          $(".seminar_list--content_op").eq(i).css({"display":"table-cell"});
          $(".seminar_list--content_op").eq(i).html("担当アドバイザーにご相談ください");
          $(".seminar_list--content_op").eq(i).addClass("seminar_list--content_op-adviser");
        }
      };
    })();
  })
  var window_size = $(window).width();
  if(window_size < 600) {
    $(window).on('load',function(){
    (function optag(){
      var box_len = $(".seminar_list--box").length;
      for( var i = 0; i<box_len; i++) {
        var target_id = $(".seminar_list--box").eq(i).attr("id");
        if( target_id == "OP") {
          $(".seminar_list--content_op").eq(i).css({"display":"block"});
        } else if(target_id == "EV"){
          $(".seminar_list--content_op").eq(i).css({"display":"block"});
          $(".seminar_list--content_op").eq(i).html("担当アドバイザーにご相談ください");
          $(".seminar_list--content_op").eq(i).addClass("seminar_list--content_op-adviser");
        }
      };
    })();
  })
  }


  $(window).on('load',function(){
    (function matchheight() {
      $('.seminar_list--table_item.seminar_list--table_item-sml').matchHeight({
    byRow: true,
  });
  })();
})
  
  $(window).on('load',function(){
    (function boxtoggle() {
      $('.seminar_list--box').on('click',function(){
        $(this).children('.seminar_list--table_list').slideToggle(300);
        $(this).children(".seminar_list--content").toggleClass('seminar_list--content-open');
      });
    })();
  })

});
