$(document).ready(function(){
  var categoryArray = [], //カテゴリ配列
    activeCatArray = [];//アクティブカテゴリ

  $.ajax({
    type: 'GET',
    url: 'https://tokyo-jobcenter.herokuapp.com/special_site/middle/seminars?callback=callback',
    dataType: 'jsonp',
    jsonpCallback: 'callback',
    success: function(data){
      var len = data.length;
      for(var i=0; i<len; i++){
        // jsonファイルを元にtableに当て込み
        $("#select_list_seminar").append(
          '<option class="' + data[i].seminar.name.slice(0,2) + '" value="' + data[i].start_at.slice(0,10) + data[i].seminar.title + '">'+ data[i].start_at.slice(0,10) + '&nbsp;&nbsp;' + data[i].seminar.title + '</option></select>'
        )
      }
    }
  });


  $(window).on('load',function(){
    (function select_seminar(){
      var item = $("#select_list_seminar option");
      var box_len = item.length;
      setTimeout(function() {
      for( var i = 0; i<box_len; i++ ) {
        var target = item.eq(i).attr("class");
        if( item.eq(i).hasClass('OP') ) {
        } else {
          item.eq(i).remove();
        }
      }
      },500);
    })();
  })



});
