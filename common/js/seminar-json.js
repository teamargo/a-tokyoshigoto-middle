$(document).ready(function(){
  var categoryArray = [], //カテゴリ配列
    activeCatArray = [];//アクティブカテゴリ

    $.ajax({
      type: 'GET',
      url: 'https://tokyo-jobcenter.herokuapp.com/special_site/middle/seminars?callback=callback',
      dataType: 'jsonp',
      jsonpCallback: 'callback',
      success: function(data){
        var len = data.length;
        for(var i=0; i<len; i++){

        // jsonファイルを元にtableに当て込み
        $("#seminar_list").append(
          '<div class="seminar_list--box" data-target="' + data[i].seminar.themes + '" data-number="'+ data[i].seminar.name + '" data-date="' + data[i].apply_start_on + '" data-enddate="' + data[i].apply_end_on + '" >' +
          '<ul class="seminar_stamp seminar_stamp-category">' +
          '<li class="seminar_stamp--item seminar_stamp--item-ttl">' + data[i].seminar.name + '</li>' +
          '<li class="seminar_stamp--item seminar_stamp--item-content">' + data[i].seminar.category + '</li>' +
          '</ul>' +
          '<div class="seminar_list--ttl">' +
          '<p class="seminar_list--ttl-lead">' + data[i].seminar.lead +
          '<h4>' + data[i].seminar.title + '</h4>' +
          '</div>' +
          '<dl class="seminar_list--content">' +
          '<dt class="seminar_list--content_ttl">' + '<span class="seminar_list--content_ttl-date">開催日時</span><br>' +
          '<span class="seminar_list--content_ttl-cal">' + data[i].start_at.slice(0,10) + '</span>' + '<br>' + data[i].start_at.slice(11,16) + '</dt>' +
          '<dd class="seminar_list--content_txt">' + data[i].seminar.outline + '</dd>' +
          '<dd class="seminar_list--content_ttl seminar_list--content_op">' +  '<a class="seminar_list--content_op-link" href="/contact/seminar.html" target="_blank">' + 'お申し込みは<br>こちら' + '</a>' + '</dd>' +
          '</dl>' +
          '<ul class="seminar_stamp seminar_stamp-term">' +
          '<li class="seminar_stamp--item seminar_stamp--item-ttl">' + '実施時間' + '</li>' +
          '<li class="seminar_stamp--item seminar_stamp--item-content span_time">' + data[i].seminar.schedule + '</li>' +
          '</ul>' +
          '<ul class="seminar_list--table_list">' +
          '<li class="seminar_list--table_item">' +
          '<dl class="seimnar_list--table_in">' +
          '<dt class="seminar_list--table_in_ttl">' + '対象者' + '</dt>' +
          '<dd class="seminar_list--table_in_data">' + data[i].seminar.target + '</dd>' +
          '</dl>' +
          '</li>' +
          '<li class="seminar_list--table_item seminar_list--table_item">' +
          '<dl class="seimnar_list--table_in">' +
          '<dt class="seminar_list--table_in_ttl">' + 'テーマ' + '</dt>' +
          '<dd class="seminar_list--table_in_data">' + data[i].seminar.themes + '</dd>' +
          '</dl>' +
          '</li>' +
          '<li class="seminar_list--table_item seminar_list--table_item">' +
          '<dl class="seimnar_list--table_in">' +
          '<dt class="seminar_list--table_in_ttl">' + '開催場所' + '</dt>' +
          '<dd class="seminar_list--table_in_data">' + data[i].address + '</dd>' +
          '</dl>' +
          '</li>' +
          '<li class="seminar_list--table_item seminar_list--table_item">' +
          '<dl class="seimnar_list--table_in">' +
          '<dt class="seminar_list--table_in_ttl">' + '申込方法' + '</dt>' +
          '<dd class="seminar_list--table_in_data">' + data[i].seminar.entry_method + '</dd>' +
          '</dl>' +
          '</li>' +
          '<li class="seminar_list--table_item seminar_list--table_item-md">' +
          '<dl class="seimnar_list--table_in">' +
          '<dt class="seminar_list--table_in_ttl">' + '申込期間' + '</dt>' +
          '<dd class="seminar_list--table_in_data">' + data[i].apply_start_on + '〜' + '<a class="web_apply" href="https://www.p-jobapp.com/middle/login" target="_blank"><span class="seminar_list--table_in_data-apply">' + '申込受付中' + '</span></a></dd>' +
          '</dl>' +
          '</li>' +
          '<li class="seminar_list--table_item seminar_list--table_item-sml">' +
          '<dl class="seimnar_list--table_in">' +
          '<dt class="seminar_list--table_in_ttl">' + '定員' + '</dt>' +
          '<dd class="seminar_list--table_in_data">' + data[i].attender_number + '名</dd>' +
          '</dl>' +
          '</li>' +
          '</ul>' +
          '</div>'
          );

        // カテゴリー分けするために、重複しないカテゴリーをプッシュ
        // 同時にカテゴリーボタンを生成
        // var catItem = data[i].seminar.themes.toString().replace(/\,/g,'，');
        // if(categoryArray.indexOf(catItem)<0){
        //   categoryArray.push(catItem);
        //   $('#sort').append(
        //     '<li class="sort--item"><a class="sort--link" data-target="' + catItem + '">' + catItem + '</a></li>'
        //   );
        // }

        //カテゴリー状態がtrueの時は受付中テキスト修正
        if(data[i].multi_appliable == true ) {
          $('.reception').eq(i).html('受付中').addClass('on');
        } else {
          $('.reception').eq(i).html('受付終了').addClass('off');
        }
      }
      //カテゴリボタンのハンドラを定義
      setBtnFunc();

      //すべてのセミナーが選択された状態に初期化
      //ローディング遅延は考慮していませんので、必要であればタイムアウトで処理してください
      activeCatArray = categoryArray.slice();
    }
  });

  //カテゴリボタンのイベントハンドラ
  function setBtnFunc(){
    $(".sort--link").on('click.btn-sort',function() {
      var target = $(this).attr('data-target');
      var list = $(".seminar_list--box");
      var num = 0;
      for( var i=0,len=list.length;i<len;i++ ) {
        if( target == "all"){
          list.eq(i).css({"display":"block"});
        } else if( list.eq(i).attr('data-target').match(target) ) {
          // 不要？
          list.eq(i).css({"display":"block"});
          // -- ここまで
        } else {
          list.eq(i).css({"display":"none"});
        }
      }


      for( var i = 0, len = list.length; i<len; i++ ) {
        if ( list.eq(i).css('display') == 'none' ) {
          num++;
          if ( i == list.length - 1) {
            if ( num == list.length ) {
              $('.search_result_announce').css({display:'block'});
            } else {
              $('.search_result_announce').css({display:'none'});
            }
          }
        }
      }

      $(".sort--link").not($(this)).removeClass("current");
      $(this).addClass("current");
      return false;
    });
  }

  $(window).on('load',function(){
    (function lochash() {
      var hash = $(location).attr('hash');
      var item = $(".seminar_list--box");
      var ary = [];
      var s = 0;
      function str_count(all, part) {
        return(all.match(new RegExp(part,'g')) || []).length;
      }
      var count = str_count(hash,"#");

      if( hash ) {
        for( var i = 0; i < count; i++ ){
          ary.push( hash.substring(s,s+9) );
          s = s+9;

          if( i == count-1 ) {
            item.css({'display':'none'});
            for( var i = 0; i<item.length; i++ ) {
              if( '#' + item.eq(i).attr('data-number') == ary[0] || '#' + item.eq(i).attr('data-number') == ary[1] || '#' + item.eq(i).attr('data-number') == ary[2] || '#' + item.eq(i).attr('data-number') == ary[3] || '#' + item.eq(i).attr('data-number') == ary[4] || '#' + item.eq(i).attr('data-number') == ary[5] || '#' + item.eq(i).attr('data-number') == ary[6] || '#' + item.eq(i).attr('data-number') == ary[7] || '#' + item.eq(i).attr('data-number') == ary[8] || '#' + item.eq(i).attr('data-number') == ary[9] ) {
                item.eq(i).css({'display':'block'});
              }
            }
          }
        }
      }
    })();
  })

  // nullの値を変換
  $(window).on('load', function(){
    var item = $('.seminar_list--box');
    var box_len = item.length;
    for(var i = 0; i<box_len; i++) {
      var span_time = item.eq(i).find(".span_time").html();
      if( span_time == 'null') {
        $(".span_time").eq(i).text('ー');
      }
    }
  });

  // EVとOPにお申し込みボタンを表示
  $(window).on('load',function(){
    (function optag(){
      var box_len = $(".seminar_list--box").length;
      for( var i = 0; i<box_len; i++) {
        var target_number = $(".seminar_list--box").eq(i).attr("data-number").slice(0,2);
        if( target_number == "OP" ) {
          $(".seminar_list--content_op").eq(i).css({"display":"table-cell"});
          $(".web_apply").eq(i).css({"display":"none"});
        } else if(target_number == "EV"){
          $(".seminar_list--content_op").eq(i).css({"display":"table-cell"});
          $(".seminar_list--content_op").eq(i).html("担当アドバイザーにご相談ください");
          $(".seminar_list--content_op").eq(i).addClass("seminar_list--content_op-adviser");
          $(".web_apply").eq(i).css({"display":"none"});
        }
      };
    })();
  })
  var window_size = $(window).width();
  if(window_size < 600) {
    $(window).on('load',function(){
      (function optag(){
        var box_len = $(".seminar_list--box").length;
        for( var i = 0; i<box_len; i++) {
          var target_number = $(".seminar_list--box").eq(i).attr("data-number").slice(0,2);
          if( target_number == "OP" ) {
            $(".seminar_list--content_op").eq(i).css({"display":"block"});
            $(".web_apply").eq(i).css({"display":"none"});
          } else if( target_number == "EV" ){
            $(".seminar_list--content_op").eq(i).css({"display":"block"});
            $(".seminar_list--content_op").eq(i).html("担当アドバイザーにご相談ください");
            $(".seminar_list--content_op").eq(i).addClass("seminar_list--content_op-adviser");
            $(".web_apoly").eq(i).css({"display":"none"});
          }
        };
      })();
    })
  }

  // 申込受付中アイコンの制御
  $(window).on('load',function(){
    (function datecomparison(){
      var box_len = $('.seminar_list--box').length;
      var today = new Date();
      var year = today.getFullYear();
      var month = today.getMonth()+1;
      var date = today.getDate();
      var today_string = year + '-' + month + '-' + date;
      console.log(today_string);
      for( var i = 0; i<box_len; i++) {
        var start_date = $('.seminar_list--box').eq(i).attr('data-date');
        var end_date = $('.seminar_list--box').eq(i).attr('data-enddate');
        console.log(start_date);
        if(end_date >= today_string && today_string >= start_date){
          console.log('表示');
        } else {
          $('.seminar_list--table_in_data-apply').eq(i).css({'display':'none'});
          console.log('非表示');
        }
      }
    })();
  });

  $(window).on('load',function(){
    (function matchheight() {
      $('.seminar_list--table_item.seminar_list--table_item-sml').matchHeight({
        byRow: true,
      });
    })();
  })

  $(window).on('load',function(){
    (function boxtoggle() {
      $('.seminar_list--box').on('click',function(){
        $(this).children(".seminar_list--table_list").slideToggle(300);
        $(this).children(".seminar_list--content").toggleClass('seminar_list--content-open');
      });
    })();
  })



});