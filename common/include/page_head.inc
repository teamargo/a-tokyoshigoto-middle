<div class="page_head">
  <div class="page_head--btns">
    <ul class="page_head--btns_list">
      <li class="page_head--btns_item"><a href="" class="page_head--btns_link page_head--btns_link-seminar">自分に合うメニューを探す</a></li>
      <li class="page_head--btns_item"><a href="https://www.p-jobapp.com/middle/login" target="_blank" class="page_head--btns_link page_head--btns_link-pjob">P-Jobマイページ（ご登録済みの方）</a></li>
      <li class="page_head--btns_item page_head--btns_item-mail"><a href="/contact/" class="page_head--btns_link page_head--btns_link-mail">お問合せ</a></li>
      <li class="page_head--btns_item page_head--btns_item-tel"><span href="" class="page_head--btns_link page_head--btns_link-tel">03-3234-1433</span></li>
      <li class="page_head--btns_item page_head--btns_item-fb">
        <a href="https://www.facebook.com/tokyomiddle" target="_blank" class="page_head--btns_link page_head--btns_link-fb"><img src="/images/img-page-head-btns-facebook.png" alt="facebook" width="48" height="48" /></a>
      </li>
    </ul>
    <div class="search_menu">
      <div class="search_menu--inner">
        <h2 class="search_menu--ttl">あなたにぴったりの支援メニューが見つかります</h2>
        <ul class="search_menu--list">
          <li class="search_menu--item"><a href="/support_menu/menu01.html" class="search_menu--link">キャリアを活かして仕事を見つけたい方</a></li>
          <li class="search_menu--item"><a href="/support_menu/menu02.html" class="search_menu--link">新たな可能性を視野に入れてこれからの働き方を考えたい方</a></li>
          <li class="search_menu--item"><a href="/support_menu/menu03.html" class="search_menu--link">求職者同士の情報交換を通じて早期就職を目指したい方</a></li>
          <li class="search_menu--item"><a href="/support_menu/menu04.html" class="search_menu--link">就職活動スキルを補強したい方</a></li>
          <li class="search_menu--item"><a href="/support_menu/menu05.html" class="search_menu--link">離職から半年以上経過している方</a></li>
          <li class="search_menu--item"><a href="/support_menu/menu06.html" class="search_menu--link">転職をお考えの方｜在職中の方（契約期限が近い方を含む）</a></li>
          <li class="search_menu--item"><a href="/support_menu/menu07.html" class="search_menu--link">正社員を目指したい方（30〜44歳）</a></li>
          <li class="search_menu--item"><a href="/support_menu/menu08.html" class="search_menu--link">キャリアを生かしづらいと感じている方（45〜54歳）</a></li>
        </ul>
        <p class="search_menu--close">閉じる</p>
      </div>
    </div>
  </div>
</div>