<div class="nav_blk">
  <a href="/" class="logo"><img src="/images/img-logo.svg" width="175" height="125" alt="東京しごとセンター　ミドルコーナー　30歳〜54歳の求職サイト"/></a>
  <div class="nav_blk_btn">
    <ul class="nav_blk_btn--list">
      <li class="nav_blk_btn--item nav_blk_btn--item-seminar"><img src="/images/icon-nav-blk-btn-seminar.png" alt="" /></li>
      <li class="nav_blk_btn--item nav_blk_btn--item-login">
        <a href="https://www.p-jobapp.com/middle/login" target="_blank"><img src="/images/icon-nav-blk-btn-login.png" alt="" /></a>
      </li>
    </ul>
    <div class="search_menu_sp">
      <h2 class="search_menu_sp--ttl">あなたにピッタリの<br/>支援メニューが見つかります</h2>
      <ul class="search_menu_sp--list">
        <li class="search_menu_sp--item"><a href="/support_menu/menu01.html" class="search_menu_sp--link">キャリアを活かして仕事を見つけたい方</a></li>
        <li class="search_menu_sp--item"><a href="/support_menu/menu02.html" class="search_menu_sp--link">新たな可能性を視野に入れて<br/>これからの働き方を考えたい方</a></li>
        <li class="search_menu_sp--item"><a href="/support_menu/menu03.html" class="search_menu_sp--link">求職者同士の情報交換を通じて<br/>早期就職を目指したい方</a></li>
        <li class="search_menu_sp--item"><a href="/support_menu/menu04.html" class="search_menu_sp--link">就職活動スキルを補強したい方</a></li>
        <li class="search_menu_sp--item"><a href="/support_menu/menu05.html" class="search_menu_sp--link">離職から半年以上経過している方</a></li>
        <li class="search_menu_sp--item"><a href="/support_menu/menu06.html" class="search_menu_sp--link">転職をお考えの方<br/>（契約期限が近い方を含む）</a></li>
        <li class="search_menu_sp--item"><a href="/support_menu/menu07.html" class="search_menu_sp--link">正社員を目指したい方（30〜44歳）</a></li>
        <li class="search_menu_sp--item"><a href="/support_menu/menu08.html" class="search_menu_sp--link">キャリアを生かしづらいと感じている方（45〜54歳）</a></li>
      </ul>
      <p class="search_menu_sp--close">&times;</p>
    </div>
  </div>
  <div class="hamburger"><a class="hamburger--btn"><span></span><span></span><span></span></a></div>
  <nav class="gl_nav">
    <ul class="gl_nav--list">
      <li class="gl_nav--item"><a href="/index.html" class="gl_nav--link gl_nav--link-service">サービス紹介</a></li>
      <li class="gl_nav--item"><a href="/beginner/" class="gl_nav--link gl_nav--link-beginner">はじめての方へ</a></li>
      <li class="gl_nav--item"><a href="/counseling/" class="gl_nav--link gl_nav--link-counseling">キャリアカウンセリング </a></li>
      <li class="gl_nav--item"><a href="/seminar/" class="gl_nav--link gl_nav--link-seminar">セミナー・プログラム</a></li>
      <li class="gl_nav--item"><a href="/event/" class="gl_nav--link gl_nav--link-event">イベント</a></li>
      <li class="gl_nav--item"><a href="/recruit/" class="gl_nav--link gl_nav--link-recruit">求人紹介</a></li>
      <li class="gl_nav--item"><a href="/beginner/index.html#flow_ttl" class="gl_nav--link gl_nav--link-staff">利用方法・アクセス</a></li>
    </ul>
    <a href="" class="youtube_btn"><img src="/images/img-frame-youtube.jpg" alt="" /></a><a href="https://www.youtube.com/embed/ISuy9qXlZ8Q" target="_blank" class="btn_promote btn_promote-yt sp">Youtube</a>
    <a href="https://www.facebook.com/tokyomiddle" target="_blank" class="btn_promote btn_promote-fb sp">Facebook</a>
    <div class="sns">
      <ul class="sns--list">
        <li class="sns--item">
          <div class="fb-like" data-href="https://tokyoshigoto-middle.jp/" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
        </li>
        <li class="sns--item"><a href="https://twitter.com/share" data-url="http://tokyoshigoto-middle.jp" data-text="30歳〜54歳の就職・転職活動をサポート｜東京しごとセンター　ミドルコーナー" data-lang="ja" class="twitter-share-button">ツイート</a>
          <script>
            ! function(d, s, id)
            {
              var js, fjs = d.getElementsByTagName(s)[0],
                p = /^http:/.test(d.location) ? 'http' : 'https';
              if (!d.getElementById(id))
              {
                js = d.createElement(s);
                js.id = id;
                js.src = p + '://platform.twitter.com/widgets.js';
                fjs.parentNode.insertBefore(js, fjs);
              }
            }(document, 'script', 'twitter-wjs');
          </script>
        </li>
        <li class="sns--item"><span><script type="text/javascript" src="//media.line.me/js/line-button.js?v=20140411"></script><script type="text/javascript">new media_line_me.LineButton({"pc":false,"lang":"ja","type":"a"});</script></span></li>
      </ul>
    </div>
    <p class="nav_blk--txt">東京しごとセンター「ミドルコーナー」は、30歳から54歳の就職・転職活動を無料でサポートします。</p>
    <ul class="btn_contacts">
      <li class="btn_contacts--item">
        <a href="/contact/" class="btn_contacts--link btn_contacts--link-mail"><img src="/images/img-page-head-btns-link-mail.png" alt="" class="btn_contacts--link_pic btn_contacts--link_pic-mail" /></a><span class="btn_contacts--link-bold">お問合せ</span></li>
      <li class="btn_contacts--item">
        <a href="tel:0332341433" class="btn_contacts--link btn_contacts--link-tel"><img src="/images/img-page-head-btns-link-tel.png" alt="" class="btn_contacts--link_pic btn_contacts--link_pic-tel" /></a><a href="tel:0332341433" class="btn_contacts--link-bold">03-3234-1433</a></li>
    </ul>
  </nav>
</div>