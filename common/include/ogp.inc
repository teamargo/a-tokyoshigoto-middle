<meta property="og:title" content="東京しごとセンター　ミドルコーナー">
    <meta property="og:description" content="30歳〜54歳の方を対象に、専任の就職支援アドバイザーによる就職相談や、毎日開催している多彩なセミナー・プログラムなどで就職・転職活動をサポートする東京しごとセンター ミドルコーナーのスペシャルサイトです。">
    <meta property="og:site_name" content="東京しごとセンター　ミドルコーナー">
    <meta property="og:type" content="website">
    <meta property="og:image" content="https://tokyoshigoto-middle.jp/images/og-imgage.png">
    <meta property="og:url" content="https://tokyoshigoto-middle.jp/">